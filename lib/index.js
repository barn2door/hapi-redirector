'use strict';

var Joi = require('joi');
var Hoek = require('hoek');

var internals = {};

internals.schema = Joi.object().keys({
    redirectToHttps: Joi.boolean().optional(),
    redirectToWww: Joi.array().items(Joi.string()).optional()
});

internals.defaults = {
    redirectToWww: [],
    redirectToHttps: false,
};

exports.register = function(server, options, next) {

    if(['http', 'https'].indexOf(server.info.protocol) < 0) {
        return next(new Error('Plugin is only compatible with http and https hapi servers'));
    }

    // Ensure options passed in are valid and merge with defaults
    var validateOptions = internals.schema.validate(options);
    if (validateOptions.error) {
        return next(validateOptions.error);
    }

    var settings = Hoek.applyToDefaults(internals.defaults, options);

    // Determine if the settings could cause a redirect
    var hasPotentialRedirect = settings.redirectToHttps || settings.redirectToWww.length > 0;

    server.ext('onRequest', function (request, reply) {

        if(hasPotentialRedirect !== true) {
            return reply.continue();
        }

        var host = request.headers.host.toLowerCase();
        var protocol = server.info.protocol;
        var redirectNeeded = false;

        if(settings.redirectToWww.indexOf(host) > -1) {
            host = 'www.' + host;
            redirectNeeded = true;
        }

        if(settings.redirectToHttps && request.headers['x-forwarded-proto'] === 'http') {
            protocol = 'https';
            redirectNeeded = true;
        }

        if(redirectNeeded) {
            return reply('Redirecting')
                .redirect(protocol + '://' + host + request.url.path)
                .code(301);
        }
        else {
            return reply.continue();
        }
    });

    next();
};

exports.register.attributes = {
    pkg: require('../package.json')
};
