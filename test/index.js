'use strict';

var Lab = require('lab');
var lab = exports.lab = Lab.script();
var describe = lab.describe;
var it = lab.it;
var expect = require('code').expect;

var Hapi = require('hapi');

describe('hapi-redirector', function () {

    // Self-signed ssl cert and key. Necessary to create a
    // https hapi server
    var tlsOptions = {
        key: '-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEAuxDFKtcaZdKAFeFPKnUvlLmEOOjAHMY8LWOl6bzP3O6tZuN9\nJl8bgM2Kz3mx6/fKSL84qcyI2vn6F0Uqx04g57E+TGwOyjm3yY4Jpovpxp4pP/5r\npSjR8bRtMbHEZmL4EjnHpqYEXYx37/Tdpn/StYHnMI9paVLJueaL57F7XMAHPuxa\nujpylz4+VmPMKLMcOu6IRLZ0un0e6xAQNvGp9KNCCYb2WqS7dQHVhD0EYE6QNLhb\n0kpJ0uqnVyVAJiXlt5Y4M0AEivWf1SV8PSJThMR51DfvaUFtVJjgLEm3Zg61GxUd\nkmborT2h9xvrxHuGrnuUXX1krS4r6ZS/JlaqjQIDAQABAoIBAQCXUJVsddqwinl5\nOWtkGK6ISxgdQIhFvkRnIrfOPiCVwasaEuHk7AG/SoQCF5pIGYPTHHqlPwFkv9gA\ngr57wUL0p3aRMscz5UQ5EV/VDE/pjX4oeZBxZHeOYDgDfpwzhdLMS1ioAJp/YWST\nAStZxwOWNqHfseG+hG0QpuB8ZiA3gOiW/Vqu3sP0GhZ6Qs/JguNxKkKhIkTvooKq\nLzkEUsRUvZU0hEeCB6ZL2gCMgZYK40fgzsLvQp+DrchOVLT9bh0Vv5Qfrsd7Jqa9\nma4GBhcvTdWcY+jadKaxaxjCbwrQlwKv2dmJOEJmwsGDazmtQFhyB2uKNFzctUtu\nIr16/IiBAoGBAPfKSo/l4Epp0I/zR+giuISMY8wN+WaxCZZ9RUMvuLRMrDImSFkR\n5NuAlbH/HQXXbfJJqtnKRjcrcE1nTD9Gdido/rbbYNVxz7xrg4ftKy5TzWu7r7VW\na70Meq6U5XscF1+M4fKKWr3paw5Eu5Us6m2WJe1K4brEyE58AWbOwggFAoGBAMFD\nbH2GNd39G7qJFCd+cTW1d+IndHQxKx/AA04fLQTXVsPs7yp+vixkYPPfq3wgo8uo\nsDEV/lKPna5ZVFxxQlLBCcWcZG6yoCvUGj1AoK3n59RPTllV2vTYSWnLbksWSThG\n/0IedxXF02zwB9g41Fl3EBFELKM9DRsC61o1HEbpAoGABy8KMlnLwtyGe2XmM8it\n6MnZKtVM4blIOeCv3V7I0BFY/Ks3db2bEPINZPesc7rnKRsRpPbESl0sCFJyV3NH\ngaT9nXeGuyltiFzll0887T7oubLbSTqULQrrk5wlewg5dT+0XT+9mbvmqqc7ePuw\nYDT2LIb7PFrwI7ZcpApifbECgYEAlg2Hep5tzIHsuZQc9d0SgST4g7smi+BxHyGy\nb0F9brjTQSQBPflSJp15OWB8gM/G3SWIR1RQNkITp37sJSo62I3QzIF06Q3/logE\nJ3K6Tsw0D3/T0UD71AbqWR398IbJKszU+70zsM/8LCThslvNY/kxnHZWQhfwyPwn\nR87cuaECgYAqGtkmazwPTvPw+UIo0cH79GalPz+A5nWFMWtPMi+ZxnmMx97MkvL+\neWrzpny2bx9g0HSUl0p9khqQq9Wineq44rNobt3bCM6kyeP4oG/7Eb+V7qdNKALz\nz4mvVw35K674NtD3PqNlydtLNoF3C4i3rcTZu2/bwXL2RP59+PWdaQ==\n-----END RSA PRIVATE KEY-----\n',
        cert: '-----BEGIN CERTIFICATE-----\nMIIC/DCCAeQCCQCBTWD1jXMg2DANBgkqhkiG9w0BAQUFADBAMQswCQYDVQQGEwJV\nUzELMAkGA1UECAwCV0ExEDAOBgNVBAcMB1NlYXR0bGUxEjAQBgNVBAoMCUJhcm4y\nRG9vcjAeFw0xNTAyMjUyMjE2NTRaFw0xNjAyMjUyMjE2NTRaMEAxCzAJBgNVBAYT\nAlVTMQswCQYDVQQIDAJXQTEQMA4GA1UEBwwHU2VhdHRsZTESMBAGA1UECgwJQmFy\nbjJEb29yMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuxDFKtcaZdKA\nFeFPKnUvlLmEOOjAHMY8LWOl6bzP3O6tZuN9Jl8bgM2Kz3mx6/fKSL84qcyI2vn6\nF0Uqx04g57E+TGwOyjm3yY4Jpovpxp4pP/5rpSjR8bRtMbHEZmL4EjnHpqYEXYx3\n7/Tdpn/StYHnMI9paVLJueaL57F7XMAHPuxaujpylz4+VmPMKLMcOu6IRLZ0un0e\n6xAQNvGp9KNCCYb2WqS7dQHVhD0EYE6QNLhb0kpJ0uqnVyVAJiXlt5Y4M0AEivWf\n1SV8PSJThMR51DfvaUFtVJjgLEm3Zg61GxUdkmborT2h9xvrxHuGrnuUXX1krS4r\n6ZS/JlaqjQIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQAXwqG8kHdR8hW38yu1ew/u\nHd2fw278jHgUunMKezmq4Fylqir0kySD79F54p5MPWjkSTibas+HxmZ2w1aDndmH\ngJldd3jubSGOdU4SGOw304xyoLYc+4m1Ehp205tvbZNHufbQf4lBg6UC7jiFXbyi\nhkzrztw/NJSjAL4NEYuB+5Wd8irhSJH6T+DuzYxeXmrW4NF2kEeh1G0pseKCPOAz\nrho4l+H4qwQ8J5OX6fWTpafSYEMnH5SvQmFB0mVZEgH9OtFKwi+Qck13tWfAQCMp\nlZoX/cSm4hy8MTI6d0qdzFmq/lM504gXV29mKamM40C7hjKnjAiwdi7dKUJI0cOu\n-----END CERTIFICATE-----\n'
    };

    describe('protocols', function () {

        it('should throw when hapi protocol is not http or https', function (done) {

            var server = new Hapi.Server();
            server.connection({ port: '/'});
            server.register(require('../'), function (err) {
                expect(err).to.exist();
                expect(err).to.be.an.instanceOf(Error);
                expect(err.message).to.equal('Plugin is only compatible with http and https hapi servers');
                done();
            });
        });

        it('should not throw when hapi protocol is http', function (done) {

            var server = new Hapi.Server();
            server.connection({ port: '3000'});
            server.register(require('../'), function (err) {
                expect(err).to.not.exist();
                done();
            });
        });

        it('should not throw when hapi protocol is https', function (done) {

            var server = new Hapi.Server();
            server.connection({ port: '3000', tls: tlsOptions});
            server.register(require('../'), function (err) {
                expect(err).to.not.exist();
                done();
            });
        });
    });

    describe('https redirection', function () {
        var server = new Hapi.Server();
        server.connection({ host: 'localhost', port: 3000 });

        var plugin = {
            register: require('../'),
            options: {
                redirectToHttps: true
            }
        };

        server.register(plugin, function (err) {
            if (err) {
                throw err;
            }
        });

        server.route({
            method: 'GET',
            path: '/',
            handler: function (request, reply) {
                reply('Hi from the Barn!');
            }
        });

        it('redirects requests where x-forwarded-proto is http', function (done) {

            server.inject({
                url: '/',
                headers: {
                    host: 'host',
                    'x-forwarded-proto': 'http'
                }
            }, function (response) {
                expect(response.statusCode).to.equal(301);
                expect(response.headers.location).to.equal('https://host/');
                done();
            });
        });

        it('redirects requests where x-forwarded-proto is http and includes query string', function (done) {
            server.inject({
                url: '/?test=test&test2=test2',
                headers: {
                    host: 'host',
                    'x-forwarded-proto': 'http'
                }
            }, function (response) {
                expect(response.statusCode).to.equal(301);
                expect(response.headers.location).to.equal('https://host/?test=test&test2=test2');
                done();
            });
        });

        it('ignores all other requests', function (done) {
            server.inject({
                url: '/',
                headers: {
                    host: 'host',
                    'x-forwarded-proto': 'https'
                }
            }, function (response) {
                expect(response.statusCode).to.equal(200);
                expect(response.result).to.equal('Hi from the Barn!');
                done();
            });
        });
    });

    describe('www redirection', function () {
        var server = new Hapi.Server();
        server.connection({ host: 'localhost', port: 3000 });

        var plugin = {
            register: require('../'),
            options: {
                redirectToWww: ['host']
            }
        };

        server.register(plugin, function (err) {
            if (err) {
                throw err;
            }
        });

        server.route({
            method: 'GET',
            path: '/',
            handler: function (request, reply) {
                reply('Hi from the Barn!');
            }
        });

        it('redirects to www when host matches', function (done) {

            server.inject({
                url: '/',
                headers: {
                    host: 'host'
                }
            }, function (response) {

                expect(response.statusCode).to.equal(301);
                expect(response.headers.location).to.equal('http://www.host/');
                done();
            });
        });

        it('does not redirect to www when host does not match', function (done) {

            server.inject({
                url: '/',
                headers: {
                    host: 'somehost'
                }
            }, function (response) {

                expect(response.statusCode).to.equal(200);
                expect(response.result).to.equal('Hi from the Barn!');
                done();
            });
        });

        it('does not change protocol when redirecting to www', function (done) {

            var httpsServer = new Hapi.Server();

            httpsServer.connection({ host: 'localhost', port: 3000, tls: tlsOptions });

            var plugin = {
                register: require('../'),
                options: {
                    redirectToWww: ['host']
                }
            };

            httpsServer.register(plugin, function (err) {
                if (err) {
                    throw err;
                }
            });

            httpsServer.route({
                method: 'GET',
                path: '/',
                handler: function (request, reply) {
                    reply('Hi from the Barn!');
                }
            });

            httpsServer.inject({
                url: '/',
                headers: {
                    host: 'host'
                }
            }, function (response) {

                expect(response.statusCode).to.equal(301);
                expect(response.headers.location).to.equal('https://www.host/');
                done();
            });
        });
    });

    describe('combined', function () {

        it('redirects to www and ssl', function (done) {

            var server = new Hapi.Server().connection({ host: 'localhost', port: 3000 });

            var plugin = {
                register: require('../'),
                options: {
                    redirectToWww: ['host'],
                    redirectToHttps: true
                }
            };

            server.register(plugin, function (err) {
                if (err) {
                    throw err;
                }
            });

            server.route({
                method: 'GET',
                path: '/',
                handler: function (request, reply) {
                    reply('Hi from the Barn!');
                }
            });

            server.inject({
                url: '/',
                headers: {
                    host: 'host',
                    'x-forwarded-proto': 'http'
                }
            }, function (response) {

                expect(response.statusCode).to.equal(301);
                expect(response.headers.location).to.equal('https://www.host/');
                done();
            });
        });

        it('default options have no effect', function (done) {

            var server = new Hapi.Server().connection({ host: 'localhost', port: 3000 });

            var plugin = {
                register: require('../'),
            };

            server.register(plugin, function (err) {
                if (err) {
                    throw err;
                }
            });

            server.route({
                method: 'GET',
                path: '/',
                handler: function (request, reply) {
                    reply('Hi from the Barn!');
                }
            });

            server.inject({
                url: '/',
                headers: {
                    host: 'host',
                    'x-forwarded-proto': 'http'
                }
            }, function (response) {

                expect(response.statusCode).to.equal(200);
                expect(response.result).to.equal('Hi from the Barn!');
                done();
            });
        });

        it('should error on invalid options', function (done) {

            var server = new Hapi.Server();
            server.connection({ port: 3000});
            server.register({ register: require('../'), options: { redirectToHttps: 'evil'}}, function (err) {
                expect(err).to.exist();
                expect(err).to.be.an.instanceOf(Error);
                expect(err.name).to.equal('ValidationError');
                done();
            });
        });
    });
});
